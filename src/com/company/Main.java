package com.company;

import model.Match;
import model.Delivery;
import java.util.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

public class Main {

    public static final int COUNT = 1;
    public static final int flag = 0;
    public static final String DELIMITER = ",";
    public static final int MATCH_ID = 0;
    public static final int MATCH_SEASON = 1;
    public static final int MATCH_WINNER = 10;
    public static final String MATCH_FILE_PATH = "src/data/matches.csv";

    public static final int DELIVERY_ID = 0;
    public static final int DELIVERY_BOWLING_TEAM = 2;
    public static final int DELIVERY_EXTRA_RUN = 16;
    public static final String DELIVERY_FILE_PATH = "src/data/deliveries.csv";
    public static final String MATCHES_PLAYED_IN_2016 = "2016";
    public static final int DELIVERY_BOWLERS = 8;
    public static final int DELIVERY_TOTAL_RUNS = 17;
    public static final String MATCHES_PLAYED_IN_2015 = "2015";
    public static final int DELIVERY_BATTING_TEAMS = 2;
    public static final int DELIVERY_BATSMAN = 6;
    public static final String SUNRISERS_HYDERABAD = "Sunrisers Hyderabad";
    public static String flag_string = "";


    public static void main(String[] args) {
        List<Match> matches = getMatchData();
        List<Delivery> deliveries = getDeliveryData();

        noOfMatchPlayedPerYear(matches);
        noOfMatchesWonOfAllTeamsOverAllTheYears(matches);
        year2016GetTheExtraRunsConcededPerTeam(matches, deliveries);
        year2015GetTheTopEconomicalBowlers(matches, deliveries);
        TopBatsmanForRoyalChallengersBangalore(deliveries);
    }

    private static void TopBatsmanForRoyalChallengersBangalore(List<Delivery> deliveries) {
        Map<String, String> teams = new TreeMap<>(Collections.reverseOrder());
        for (Delivery delivery: deliveries) {
            if (teams.containsKey(delivery.getDeliveryTeam()) && delivery.getDeliveryTeam().equals(SUNRISERS_HYDERABAD)) {
                String totalRun = String.valueOf(Integer.parseInt(teams.get(delivery.getDeliveryTeam())+delivery.getTotalRuns()));
                teams.put(totalRun, delivery.getDeliveryTeam());
            } else if (delivery.getDeliveryTeam().equals(SUNRISERS_HYDERABAD)){
                teams.put(delivery.getTotalRuns(), delivery.getBatsman());
            }
        }
        System.out.println("Top batsman for  Sunrisers Hyderabad : "+teams.values());
    }
    private static void year2015GetTheTopEconomicalBowlers(List<Match> matches, List<Delivery> deliveries) {
        Map<String, Float> economicalBowlers = new TreeMap<>();
        Map<String, Integer> totalOversByBowler = new TreeMap<>();

        List<String> matchIds = new ArrayList<>();
        for (Match match: matches) {
            if ( match.getSeason().equals(MATCHES_PLAYED_IN_2015)) {
                matchIds.add(match.getMatchId());
            }
        }

        for (Delivery delivery: deliveries) {
            if ( matchIds.contains(delivery.getDeliveryId()) && economicalBowlers.containsKey(delivery.getBowler())) {
                economicalBowlers.put(delivery.getBowler(), economicalBowlers.get(delivery.getBowler()) +
                        Integer.parseInt(delivery.getTotalRuns()));
            } else if ( matchIds.contains(delivery.getDeliveryId()) ) {
                economicalBowlers.put(delivery.getBowler(), Float.parseFloat(delivery.getTotalRuns()));
            }

            if (matchIds.contains(delivery.getDeliveryId()) && !(totalOversByBowler.containsKey(delivery.getBowler()))){
                flag_string=delivery.getBowler();
                totalOversByBowler.put(delivery.getBowler(), COUNT);
            } else if ( matchIds.contains(delivery.getDeliveryId()) && !(flag_string.equals(delivery.getBowler())) &&
                    !(flag_string.equals(""))) {
                flag_string = delivery.getBowler();
                totalOversByBowler.put(delivery.getBowler(), totalOversByBowler.get(delivery.getBowler()) + COUNT);
            }
        }

        economicalBowlers.forEach((key, value) -> economicalBowlers.put(key, value / totalOversByBowler.get(key)));
        System.out.println("The year 2015 get the top economical bowlers: "+ economicalBowlers);
    }

    private static void year2016GetTheExtraRunsConcededPerTeam(List<Match> matches, List<Delivery> deliveries ) {
        Map<String, Integer> extraRunsPerTeams = new TreeMap<>();
        List<String> matchIds = new ArrayList<>();
        for (Match match: matches) {
            if ( match.getSeason().equals(MATCHES_PLAYED_IN_2016)) {
                matchIds.add(match.getMatchId());
            }
        }

        for (Delivery delivery: deliveries) {
            if ( matchIds.contains(delivery.getDeliveryId()) &&
                    extraRunsPerTeams.containsKey(delivery.getDeliveryTeam())) {
                extraRunsPerTeams.put(delivery.getDeliveryTeam(), extraRunsPerTeams.get(delivery.getDeliveryTeam()) +
                        Integer.parseInt(delivery.getExtraRuns()));
            } else if ( matchIds.contains(delivery.getDeliveryId()) ) {
                extraRunsPerTeams.put(delivery.getDeliveryTeam(), Integer.parseInt(delivery.getExtraRuns()));
            }
        }
        System.out.println("The year 2016 get the extra runs conceded per team: " + extraRunsPerTeams);
    }

    private static void noOfMatchesWonOfAllTeamsOverAllTheYears(List<Match> matches) {
        Map<String, Integer> noOfMatchesWonByTeams = new TreeMap<>();
        for (Match match : matches) {
           if (noOfMatchesWonByTeams.containsKey(match.getWinningTeam())) {
                noOfMatchesWonByTeams.put(match.getWinningTeam(), noOfMatchesWonByTeams.get(match.getWinningTeam()) +
                        COUNT);
           } else if (match.getWinningTeam().length()!= flag) {
               noOfMatchesWonByTeams.put(match.getWinningTeam(), COUNT);
           }
        }
        System.out.println("Number of matches won of all teams over all the years of IPL: " + noOfMatchesWonByTeams);
    }
    
    private static void noOfMatchPlayedPerYear(List<Match> matches) {
        Map<String, Integer> noOfMatchesSeasonWise = new TreeMap<>();
        for (Match match : matches) {
            if (noOfMatchesSeasonWise.containsKey(match.getSeason())) {
                noOfMatchesSeasonWise.put(match.getSeason(), noOfMatchesSeasonWise.get(match.getSeason()) + COUNT);
            } else {
                noOfMatchesSeasonWise.put(match.getSeason(), COUNT);
            }
        }
        System.out.println("Number of matches played per year of all the years in IPL: " + noOfMatchesSeasonWise);
    }

    private static List<Delivery> getDeliveryData() {
        List<Delivery> deliveries = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(DELIVERY_FILE_PATH));
            String line;
            int i = flag;
            while ((line = br.readLine()) != null) {
                if (i == flag) {
                    i++;
                    continue;
                }
                Delivery delivery = new Delivery();
                String[] deliveryData = line.split(DELIMITER);
                delivery.setDeliveryId(deliveryData[DELIVERY_ID]);
                delivery.setDeliveryTeam(deliveryData[DELIVERY_BOWLING_TEAM]);
                delivery.setExtraRuns(deliveryData[DELIVERY_EXTRA_RUN]);
                delivery.setBowler(deliveryData[DELIVERY_BOWLERS]);
                delivery.setTotalRuns(deliveryData[DELIVERY_TOTAL_RUNS]);
                delivery.setBattingTeam(deliveryData[DELIVERY_BATTING_TEAMS]);
                delivery.setBatsman(deliveryData[DELIVERY_BATSMAN]);
                deliveries.add(delivery);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    private static List<Match> getMatchData() {
        List<Match> matches = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(MATCH_FILE_PATH));
            String line;
            int i = flag;
            while ((line = br.readLine()) != null) {
                if (i == flag) {
                    i++;
                    continue;
                }
                Match match = new Match();
                String[] matchesData = line.split(DELIMITER);
                match.setMatchId(matchesData[MATCH_ID]);
                match.setSeason(matchesData[MATCH_SEASON]);
                match.setWinningTeam(matchesData[MATCH_WINNER]);
                matches.add(match);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matches;
    }
}